<?php
$arr = Array();

$emi = $_GET['emi'];
$final_result  = Array();

$absolute_high =  rand(25, 35)+rand(0,10)/10.0;
$absolute_medium =  rand(15, 25)+rand(0,10)/10.0;
$absolute_low =  rand(9, 15)+rand(0,10)/10.0;
$absolute_guareenteed =  rand(0, 9)+rand(0,10)/10.0;

$absolute_high_r =  (1+$absolute_high/100)*$emi;
$absolute_medium_r =  (1+$absolute_medium/100)*$emi;
$absolute_low_r =  (1+$absolute_low/100)*$emi;
$absolute_guareenteed_r =  (1+$absolute_guareenteed/100)*$emi;
echo "
{
  'high-returns': {
    'time-period':2,
    'absolute':$absolute_high,
    'principal':$emi,
    'current-value':$absolute_high_r,
    'tax-free-percent':76,
    'risk-percentage':22.8
  },
  'medium-returns': {
    'time-period':2,
    'absolute':$absolute_medium,
    'principal':$emi,
    'current-value':$absolute_medium_r,
    'tax-free-percent':82,
    'risk-percentage':15.2
  },
  'low-returns': {
    'time-period':2,
    'absolute':$absolute_low,
    'principal':$emi,
    'current-value':$absolute_low_r,
    'tax-free-percent':58,
    'risk-percentage':3.8
  },
  'guaranteed-returns': {
    'time-period':2,
    'absolute':$absolute_guareenteed,
    'principal':$emi,
    'current-value':$absolute_guareenteed_r,
    'tax-free-percent':49,
    'risk-percentage':0
  }
}
"
?>